Install instructions from source
================================

Compilation instructions
------------------------

make the build directory and enter it
`mkdir -p build && cd build`

run cmake to generate the make file and then build the project
`cmake .. && make`

test if it's running properly
`cd .. && build/hin9`

Alpine Linux
------------

to install you first need to enable the testing repos

update the repos

`apk update`

install the apk

`apk add hinsightd`

on openrc your start by

`/etc/init.d/hinsightd`

inside a container or some other minrootfs, just start the executable

`hinsightd`



