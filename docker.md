Run under docker or similar
===========================

At the time of writing of this guide we don't provide out of the box docker images so you have to make your own. It's of a moderate difficulty but in the end it makes it easier to learn how to customize the final image. So in my opinion it a net possitive when it comes to total effort invested.

make your own docker image
--------------------------

start docker

create a directory for the image

	mkdir nice_project
	cd nice_project

write some magic to the Dockerfile

`nano Dockerfile`

	FROM alpine:edge
	
	RUN apk update && apk upgrade
	RUN apk add hinsightd
	
	CMD ["hinsightd"]

and build and start the container

	sudo docker build -t nice_project .
	sudo docker run nice_project -p 80:8080 --rm -it nice_project

that should create an image that listens on the internal 8080 port, has it's config files at /etc/hinsightd/ and the docroot for the default vhost at /var/www/localhost/htdocs/



