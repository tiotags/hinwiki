Enabling php via fcgi
=====================

you can enable fcgi by enabling the example fcgi config script which should work in 90% cases straight out of the box, if not then you need to change the socket url to something appropriate

`cd <your config directory>`

*your config directory is either /etc/hinsightd/ if installed by package manager or <your sources dir>/workdir if installed from source

`mv config/_10_fcgi.lua config/10_fcgi.lua`

and restart the server

for openrc `/etc/init.d/hinsightd reload`

