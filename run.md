Run
===

Once installed simply running as `hinsightd` or `build/hin9` would use the following paths

* config file: workdir/main.lua
* access log: workdir/logs/access.log
* error/debug log: workdir/logs/hindsight.log (hindsight not hinsightd!)
* htdocs: htdocs/
* ssl: workdir/ssl/{cert,key}.pem
* http cache: /tmp/cache/

and listen on port 8080 (and port 8081 if it finds a ssl cert)
